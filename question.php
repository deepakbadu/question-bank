<?php
    include './connection/connection.php';
    include_once './layout/navbar.php';
?>
  <!-- <div class="container">
    <div class="alert alert-primary text-center" role="alert">

    </div>
  </div> -->

  <?php
    $semester = $_GET['semester'];
    $sql = "SELECT users.name, question_details.semester, question_files.questions, users.college
    FROM ((users
    INNER JOIN question_details ON users.uid = question_details.uid)
    INNER JOIN question_files ON question_details.qid = question_files.qid) WHERE semester = $semester AND approve = 1 ORDER BY qfid DESC ";
          $result = $conn->query($sql);
          $count = 0;
            if($result->num_rows>0) {
                while($row = $result->fetch_assoc()) {
                  $name = $row['name'];
                  $semester = $row['semester'];
                  $college = $row['college'];
                  $imageURL = '../images/'.$row["questions"];
                
        ?> 
          <!-- Page Content -->
          <div class="container">
            <div class="card border-3 shadow my-5">
              <div class="mx-2 mt-2 text-right">
                <img src="<?php echo $imageURL; ?>" onClick="openFullScreen(<?php echo $count++ ?>)" 
                  class="img-fluid questionImage" alt="" />
                  <hr/>
                  <div class="my-2">
                    <b>Uploaded by:</b>
                    <span><?php echo $name; ?></span>
                  </div>
                  <div class="my-2">
                    <b>College:</b>
                    <span><?php echo $college; ?></span>
                  </div>
                  <div>
                </div>
            </div>
          </div>
    <?php
                }
            }
    ?>
<script>
var elem = document.querySelectorAll(".questionImage");
function openFullScreen(count) {
  if (elem[count].requestFullscreen) {
    elem[count].requestFullscreen();
  }
}

</script>