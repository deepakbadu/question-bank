<?php
include '../connection/connection.php';

$table1 = "CREATE TABLE users(
    uid INT(11) NOT NULL AUTO_INCREMENT,
    name VARCHAR(50) NOT NULL,
    email varchar(50),
    college varchar(50),
    PRIMARY KEY (uid),
    reg_date TIMESTAMP
  )";

    if ($conn->query($table1) === TRUE) {
        echo "Table user successfully";
    } else {
        echo "Error creating table: " . $conn->error;
    }
  
    $table2 = "CREATE TABLE question_details(
        qid INT(11) NOT NULL  AUTO_INCREMENT,
        semester varchar(50) NOT NULL,
        uid int,
        PRIMARY KEY (qid),
        FOREIGN KEY (uid) REFERENCES users(uid)
        ON DELETE CASCADE
      )";
    
    if ($conn->query($table2) === TRUE) {
        echo "Table  question_details successfully";
    } else {
        echo "Error creating table: " . $conn->error;
    }

$table3 = "CREATE TABLE question_files(
    qfid INT(11) NOT NULL AUTO_INCREMENT,
    questions varchar(100),
    PRIMARY KEY (qfid),
    qid INT(11),
    FOREIGN KEY (qid) REFERENCES question_details(qid)
    ON DELETE CASCADE
 )";

if ($conn->query($table3) === TRUE) {
    echo "Table3  question files successfully";
} else {
    echo "Error creating table: " . $conn->error;
}

$altertable = " ALTER TABLE question_files
ADD COLUMN approve BOOLEAN DEFAULT FALSE ";

if ($conn->query($altertable) === TRUE) {
    echo "alter  question files successfully";
} else {
    echo "Error creating table: " . $conn->error;
}

$conn->close();


?>