<?php
  session_start();
  include_once 'layout/navbar.php';
  include 'notification.php';
?>

<div class="container ">
  <?php
    displayNotification();
  ?>
<div class="card text-center">
  <div class="card-header">
    <h3> Welcome </h3>
  </div>
  <div class="card-body">
    <h5 class="card-title">Please support us by uploading past exam questions.</h5>
    <p class="card-text">Browse through the semester section to view questions.</p>
  </div>
  <div class="card-footer bg-transparent border-success">Thank You!!</div>
</div>
</div>

