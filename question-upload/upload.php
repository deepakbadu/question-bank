
  <?php
    session_start();
    require '../layout/navbar.php';
    include '../notification.php';
  ?>
        <div class="container">
            <?php
                displayNotification();
            ?>
        <form class="form-horizontal" role="form" method="POST"  action="user-verification.php" enctype="multipart/form-data">
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <h2>Upload Question</h2>
                    <hr/>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <div class="form-group has-danger">
                        <label class="sr-only" for="username">Name</label>
                        <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                            <input type="text" name="name" class="form-control" id="name"
                                  placeholder="Enter name" required/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="sr-only" for="college">college</label>
                        <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                            <input type="text" name="college" class="form-control" id="college"
                                   placeholder="Enter College" required>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="sr-only" for="Email">Email</label>
                        <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                            <input type="email" name="email" class="form-control" id="email"
                                   placeholder="Enter Email" required>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleFormControlSelect1">Semester</label>
                        <select class="form-control" id="exampleFormControlSelect1" name="semester" >
                            <option disabled> Choose Your semester</option>
                            <option value="1"> 1st semester </  option>
                            <option value="2"> 2nd semester </option>
                            <option value="3"> 3rd semester </option>
                            <option value="4"> 4th semester </option>
                            <option value="5"> 5th semester </option>
                            <option value="6"> 6th semester </option>
                            <option value="7"> 7th semester </option>
                            <option value="8"> 8th semester </option>

                        </select>
                    </div>
                </div>    
            </div>      
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="profilePicture">Upload Question</label>
                        <input type="file" class="form-control-file" id="files" name="files[]" multiple/>        
                    </div>
                </div>    
            </div>
            <div class="row" >
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <button type="submit" name="submit" onclick="validate();" class="btn btn-success">Register</button>
                </div>
            </div>
        </form>
    <script>
        function valida te(){
            var regex = /^[a-zA-Z ]{2,30}$/;
            var name =  document.getElemetnById('name').value;
            if(!regex.test(name)){
    alert('Invalid name given.');
}else{
    alert('Valid name given.');
}
        }
    </script>