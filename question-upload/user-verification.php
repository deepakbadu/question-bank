<?php
    session_start();
    include '../connection/connection.php';
    include '../notification.php';

    if(empty($_POST['name']) && empty($_POST['college']) && empty($_POST['email']) 
        && empty($_POST['semester']) ){
        echo 'empty fields';
    } 
    else{
        if(isset($_POST['submit'])){

            $name = $_POST['name'];
            $college = $_POST['college'];
            $email = $_POST['email'];
            $semester = $_POST['semester'];

            $insertUser = "INSERT INTO users(name,email,college) VALUES('$name','$email','$college')";
                if($conn->query($insertUser) == TRUE) {
                    $uidforusers  ="SELECT uid FROM users where name = '$name'";
                    $result = $conn->query($uidforusers);
                    if($result->num_rows>0) {
                        while($row = $result->fetch_assoc()) {
                            $uid = $row['uid'];
                        }
                        $questionDetails = "INSERT INTO question_details (semester,uid) 
                        VALUES ('$semester', '$uid')";

                        if($conn->query($questionDetails) == TRUE) {
                           $selectqid = "SELECT qid FROM question_details WHERE semester = '$semester' ";
                                $res =  $conn->query($selectqid);
                                if($res->num_rows>0) {
                                    while($rw = $res->fetch_assoc()) {
                                        $qid = $rw['qid'];
                                    }   

                                    $targetDir = '../images/';
                                    $allowTypes = array('jpg','JPG','png','jpeg');
                                    $statusMsg = $errorMsg = $insertValuesSQL = $errorUpload = $errorUploadType = '';
                                    if(!empty(array_filter($_FILES['files']['name']))){
                                        foreach($_FILES['files']['name'] as $key=>$val){
                                            // File upload path
                                            $fileName = basename($_FILES['files']['name'][$key]);
                                            $targetFilePath = $targetDir . $fileName;
                                        // Check whether file type is valid
                                            $fileType = pathinfo($targetFilePath,PATHINFO_EXTENSION);
                                            if(in_array($fileType, $allowTypes)){
                                                if(move_uploaded_file($_FILES["files"]["tmp_name"][$key], $targetFilePath)){
                                                    $insertValuesSQL .= "('".$fileName."', '".$qid."'),";
                                                }else{
                                                    $errorUpload .= $_FILES['files']['name'][$key].', ';
                                                }
                                            }else{
                                                $errorUploadType .= $_FILES['files']['name'][$key].', ';
                                            }
                                        }
                                        if(!empty($insertValuesSQL)){
                                            $insertValuesSQL = trim($insertValuesSQL,',');
                                            // Insert image file name into database
                                            $insert = $conn->query("INSERT INTO question_files (questions, qid) VALUES $insertValuesSQL");
                                            if($insert){
                                                $errorUpload = !empty($errorUpload)?'Upload Error: '.$errorUpload:'';
                                                $errorUploadType = !empty($errorUploadType)?'File Type Error: '.$errorUploadType:'';
                                                $errorMsg = !empty($errorUpload)?'<br/>'.$errorUpload.'<br/>'.$errorUploadType:'<br/>'.$errorUploadType;
                                                createNotification('questions are uploaded successfully, please wait for admin approval to vist your question');
                                                header('location: ../index.php');
                                            }else{
                                                createNotification ("Sorry, there was an error uploading your file.");
                                                header('location: ./upload.php');
                                            }
                                        }else{
                                            createNotification('Please select a file to upload.');
                                            header('location: ./upload.php');
                                        }
                                    }else{
                                        createNotification('empty question field');
                                        header('location: ./upload.php');
                                    }
                                }
                            }
                        }
                    }
                }
    }
    
?>