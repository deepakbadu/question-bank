<?php

    include '../connection/connection.php';
    include_once '../layout/navbar.php';
    include("../admin/auth.php");
    include '../notification.php';

?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style>
      .img-thumbnail{
        
      }
    </style>
  </head>
  <body class="container mt-5" >
    <?php
      displayNotification();
    ?>
    <table class="table table-striped">
    <thead>
        <tr>
        <th scope="col">id</th>
        <th scope="col">Name</th>
        <th scope="col">email</th>
        <th scope="col">college</th>
        <th scope="col">semester</th>
        <th scope="col">questions</th>
        <th scope="col">delete</th>        
        </tr>
    </thead>
    <tbody>
    <?php
         $sql = "SELECT * FROM ((users
        INNER JOIN question_details ON users.uid = question_details.uid)
        INNER JOIN question_files ON question_details.qid = question_files.qid)  WHERE approve = 1";
          $result = $conn->query($sql);
            if($result->num_rows>0) {
                while($row = $result->fetch_assoc()) {
                      $name = $row['name'];
                      $email = $row['email'];
                      $college = $row['college'];
                      $semester = $row['semester'];
                      $qfid = $row['qfid'];
                      $uid = $row['uid'];
                      $imageURL = '../images/'.$row["questions"];

         ?>
           <form action="../delete-query.php"  method="POST" >
                <th scope="row"><?php echo $qfid; ?></th>
                <td><?php echo $name  ; ?></td>
                <td><?php echo $email; ?></td>
                <td><?php echo $college; ?></td>
                <td><?php echo $semester; ?></td>
                <td><img src="<?php echo $imageURL; ?>" alt="" class="img-thumbnail" s></td>
                <input type="hidden" name = "uid" value = "<?php echo $uid; ?>"/>
                <td><input type="submit" value="delete" > </td>
              </form>
    </tbody>
    <?php
         }

        }
    ?>
     </table>
     <a href="index.php" class="btn btn-primary" >Back </a> 

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>