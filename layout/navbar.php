<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>BIM Question Bank</title>
  </head>
  <body>
  <div class="mt-3 container">
 <!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark static-top mb-5 shadow mt-3">
  <div class="container">
    <a class="navbar-brand" href="/question-bank">Bim Old Questions</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item active">
          <a class="nav-link" href="/question-bank/question-upload/upload.php">Upload Question</a>
        </li>

        <li class="nav-item dropdown active">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="ture">
            Semester
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            <a class="dropdown-item" href="/question-bank/question.php/?semester=1">1st semester</a>
            <a class="dropdown-item" href="/question-bank/question.php/?semester=2">2nd semester</a>
            <a class="dropdown-item" href="/question-bank/question.php/?semester=3">3rd semester</a>
            <a class="dropdown-item" href="/question-bank/question.php/?semester=4">4th semester</a>
            <a class="dropdown-item" href="/question-bank/question.php/?semester=5">5th semester</a>
            <a class="dropdown-item" href="/question-bank/question.php/?semester=6">6th semester</a>
            <a class="dropdown-item" href="/question-bank/question.php/?semester=7">7th semester</a>
            <a class="dropdown-item" href="/question-bank/question.php/?semester=8">8th semester</a>
          </div>
        </li>
      </ul>
    </div>
  </div>
</nav>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
  <style>
    body {
      background-color: #e7e7e7
    }
  </style>
</html>